# README #

Lambda function to upload an html file with all AWS Accounts and account information in the Organization to a specified S3 bucket. 

### Overview ###

* This function can be called only from the organization's management account or by a member account that is a delegated administrator for an AWS service.

* Required input into function.
	- {
		"s3_bucket": "BUCKET_NAME"
	  }