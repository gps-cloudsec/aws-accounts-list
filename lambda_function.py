import json
import boto3
from botocore.exceptions import ClientError
import re
import os

html = '''
<!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>
<body>

<h2>AWS Accounts</h2>

<table>
  <tr>
    <th>Name</th>
    <th>Id</th>
    <th>Email</th>
    <th>Organizational Unit</th>
  </tr>
'''


# Prints out all accounts in the json response
# Parameters:
#    - all_accounts: json containing all child accounts for current OU
#    - file: file to print to
# .   - ou_name: name of the organization/organizational unit the account is under
def print_accounts(all_accounts, ou_name, file):
    if all_accounts['Accounts']:
        for acc in all_accounts['Accounts']:
            # file.write('{}, {}, {}, {}\n'.format(acc['Name'], acc['Id'], acc['Email'], ou_name))
            file.write('  <tr>\n    <th>{}</th>\n    <th>{}</th>\n    <th>{}</th>\n    <th>{}</th>\n  </tr>\n'.format(
                acc['Name'], acc['Id'], acc['Email'], ou_name))


# Gets all accounts and nested organizational units within the current organizational unit
# Parameters:
#    - all_ou: json containing all child organizational units for current unit
#    - client: session client
#    - file: file to print to
def print_ou(all_ou, client, file):
    if all_ou['OrganizationalUnits']:
        for ou in all_ou['OrganizationalUnits']:
            children_accounts = client.list_accounts_for_parent(ParentId=ou['Id'])
            children_ou = client.list_organizational_units_for_parent(ParentId=ou['Id'])
            print_accounts(children_accounts, ou['Name'], file)
            print_ou(children_ou, client, file)


# Starts the process of finding all accounts in the organization
# Parameters:
#    - file_name: name of the file to print to
#    - p_name: name of the profile to use to establish a session
def find_accounts(bucket_name):
    try:

        session = boto3.session.Session(profile_name='master')
        # create organization and sts client
        client = session.client('organizations')
        sts_client = session.client('sts')
        s3_client = session.resource('s3')
        s3_buckets_client = session.client('s3')

        # check if the bucket exists
        bucket_response = s3_buckets_client.list_buckets()
        bucket_found = False
        for bucket in bucket_response['Buckets']:
            if bucket['Name'] == bucket_name:
                bucket_found = True

        if not bucket_found:
            print("[ERROR] S3 Bucket '{}' not found".format(bucket_name))
            return "Error: S3 Bucket '{}' not found".format(bucket_name)

        # get the account id for the current account
        account_id = (sts_client.get_caller_identity())['Account']
        root_id_pattern = re.compile("r-([a-z]|[0-9]){4,32}")

        # check if current account is the root, otherwise get the root id for the organization
        if root_id_pattern.search(account_id):
            root_id = account_id
        else:
            root = client.list_roots()
            root_id = ''
            for r in root['Roots']:
                root_id = r['Id']

        # get all children and organizations under the root
        children_accounts = client.list_accounts_for_parent(ParentId=root_id)
        children_ou = client.list_organizational_units_for_parent(ParentId=root_id)

        # write out html to file
        with open("/tmp/accounts.html", "w") as f:
            global html
            f.write(html)
            print_accounts(children_accounts, 'NA', f)
            print_ou(children_ou, client, f)
            f.write("\n</table>\n</body>\n</html>")
            f.close()

        s3_client.meta.client.upload_file('/tmp/accounts.html', bucket_name, 'accounts/accounts.html')

        os.remove('/tmp/accounts.html')

    except ClientError as e:
        if e.response['Error']['Code'] == "AccessDeniedException":
            print("[ERROR]: " + e.response['Error']['Code'])
            return "Error: Access Denied"
        else:
            print("[ERROR]: " + e.response['Error']['Code'])
            return "Error: ".format(e.response['Error']['Code'])

    return True


def lambda_handler(event, context):
    if 's3_bucket' in event.keys():

        response = find_accounts((event['s3_bucket']))

        if isinstance(response, bool):
            return {
                'StatusCode': 200,
                'body': 'Finished writing all accounts to accounts/accounts.html in S3 bucket {}'.format(
                    event['s3_bucket'])
            }
        else:
            return {
                'StatusCode': 200,
                'body': '{}'.format(response)
            }
    else:
        print("[ERROR]: s3_bucket key not found in input JSON")
        return {
            'StatusCode': 200,
            'body': "s3_bucket key not found in input JSON"
        }


# lambda_handler({"s3_bucket": "aws-acc"}, "")

